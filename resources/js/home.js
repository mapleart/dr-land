import Swiper, { Navigation, Mousewheel, Autoplay, Pagination } from 'swiper';
Swiper.use([Navigation, Mousewheel, Autoplay, Pagination]);

(() => {
    if (document.querySelectorAll(".h-congratulations__slider").length ) {
        const swiperMain = new Swiper('.h-congratulations__slider', {
            slidesPerView: 1,
            spaceBetween: 0,
            autoplay: {
                delay: 10000,
            },
            pagination: false,
            navigation: {
                nextEl: '.h-congratulations__slider__nav__next',
                prevEl: '.h-congratulations__slider__nav__prev',
            },
            loop: true,
        })
    }

    $(document).on('click', '[data-anchor]', function (e){
        e.preventDefault();
        let target = $(this).attr('href'),
            $target = $(target);

        console.log($target);
        console.log($target.offset());
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - 100
        }, 300, 'linear', function () {
          //  window.location.hash = target;
        })

    });


    $("textarea").each(function () {
        this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
    }).on("input", function () {
        this.style.height = 0;
        this.style.height = (this.scrollHeight) + "px";
    });

})();