import Vue from 'vue'
import './header'
import './home'
import '../css/style.scss';

import { InertiaProgress } from '@inertiajs/progress'
import { createInertiaApp } from '@inertiajs/vue2'
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import { ZiggyVue } from '../../vendor/tightenco/ziggy/dist/vue.m';

Vue.config.productionTip = false
InertiaProgress.init()


/**
 * Модули самописные
 */


Vue.prototype.$token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
import './plugins'


Vue.prototype.$breakpoint = {
    width: document.documentElement.clientWidth
};


import { Link } from '@inertiajs/vue2'

createInertiaApp({
    title: title => title ? `${title} - AI-Qube` : 'AI-Qube',

    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),

    setup({ el, App, props, plugin }) {
        Vue.use(plugin)
        Vue.use(ZiggyVue, Ziggy)
        Vue.use(Link);

        Vue.prototype.$route = route

        new Vue({
            render: h => h(App, props),
            data: {
                isMobile: false
            },
            created: function(){
                const breakpoint = window.matchMedia('(max-width:991px)');
                const breakpointChecker = () => {
                    let w = document.documentElement.clientWidth;
                    this.$breakpoint.width = w
                    this.isMobile = (breakpoint.matches === true)
                    console.log(123);
                }

                breakpoint.addEventListener('change', breakpointChecker);
                breakpointChecker();
            },

            methods: {

            }
        }).$mount(el)
    },
})

