import Vue from 'vue'
import { VueAjax } from './ajax'
import GLightbox from 'glightbox';

Vue.use(VueAjax);


import './comet';
import './veelidate';


$(document).on('click', '.js-open-lightbox', function(e){
    console.log('jol')
    e.preventDefault();
    const myGallery = GLightbox({
        elements: [
            {
                'href':$(this).data('src'),
                'type': 'image'
            }
        ],
        autoplayVideos: true,
    });
    myGallery.open();
});
