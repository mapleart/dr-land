import Vue from 'vue'

import { required, min, email, regex } from 'vee-validate/dist/rules'
import { extend, validate, ValidationObserver, ValidationProvider, setInteractionMode } from 'vee-validate'

extend('required', {
    ...required,
    message: 'Обязательно для заполнения'
});
extend('url', {
    validate(value, { min, textError }) {

        let expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
        let regex = new RegExp(expression);

        if (value.match(regex)) {
            return true
        } else {
            return textError || 'Должно соответствовать формату URL';
        }
    },
    params: ['textError'],
});

extend('min', {
    validate(value, { min, textError }) {
        if(value.length < min) return textError || 'Минимум '+min;
        return true;
    },
    params: ['min', 'textError'],
});

extend('max', {
    validate(value, { min, textError }) {
        if(value.length > min) return textError || 'Максимум '+min;
        return true;
    },
    params: ['min', 'textError'],
});

extend('count', {
    validate(value, { min, max, textError }) {

        if(value.length < min) return textError || 'Минимум '+min;
        if(value.length > max) return textError || 'Максимум '+max;
        return true;
    },

    params: ['min','max', 'textError'],
});

extend('length', {
    validate(value, { min, max, textError }) {

        if(value.length < min) return textError || 'Минимум '+min;
        if(value.length > max) return textError || 'Максимум '+max;
        return true;
    },

    params: ['min','max', 'textError'],
});

extend('email', {
    ...email,
    message: 'Должно соответствовать формату email адреса'
});

extend('passwordConfirm', {
    params: ['target', 'textError'],
    validate(value, { target, textError}) {
        if(value === target) return true;
        return textError || 'Password confirmation does not match'
    },
});


extend('regex', {
    validate(value, { test, textError }) {
        if(test.test(value)){
            return true;
        }
        return  textError || 'Поле заполнено неверно';
    },
    params: ['test', 'textError'],
})

extend('compareTags', {

    validate(value, { count, textError }) {

        if( parseInt(value) >= count){
            return true;
        }
        return  textError || 'Укажите теги (мин '+count+')';
    },
    params: ['count', 'textError'],
})

extend('async', {
    validate(value, { endPoint, paramName }) {
        if (value === '') return true;

        // simulate async call, fail for all logins with even length
        return new Promise((resolve, reject) => {
            paramName = paramName || 'login';

            let remoteParams = {};
            remoteParams[paramName] = value;
            window.$ajax.get(endPoint, remoteParams).then((response)=> {
                let errors = response.errors || {};
                if(Object.keys(errors).length > 0) {
                    //this.asyncErrors = errors[this.name];
                    resolve(errors[paramName][0]);
                } else {
                    //this.asyncErrors = [];
                    resolve(true);
                }

            }, ()=>{
                // this.asyncErrors = [];
                resolve(false);
            });
        })
    },
    params: ['endPoint', 'paramName'],
});



Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
