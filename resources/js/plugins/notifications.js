import Notify from 'simple-notify'
import 'simple-notify/dist/simple-notify.min.css'


export const Notifications = {

    init() {
        //$.jGrowl.defaults.closerTemplate = '<div>Закрыть все</div>';
        //$.extend(this.options, options || {});
    },

    /**
     * Отображение информационного сообщения
     */
    show( title, message, status, params = {}) {
        if ( ! title && ! message ) {
            console.warn('Необходимо указать заголовок или текст уведомления');
            return;
        }

        return {
            status: status,
            title: title ? title : 'Внимание',
            text: message ? message : '',
           // speed: 4000,
            autoclose: true,
            ...params
        }

    },

    /**
     * Отображение информационного сообщения
     */
    notice( title, message, params ) {
        console.log(title);
        console.log(message);
        new Notify(
            this.show(title, message, 'success', params)
        );
    },

    success( title, message, params ) {
        new Notify(
            this.show(title, message, 'success', params)
        );
    },

    /**
     * Отображение сообщения об ошибке
     */
    error( title, message, params ) {
        new Notify(
            this.show(title, message, 'error', params)
        );
    },

    /**
     * Отображение сообщения об ошибке
     */
    info( title, message, params ) {
        new Notify(
            this.show(title, message, 'info', params)
        );
    }
};
