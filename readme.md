Эта команда зарегистрирует текущий каталог как путь, по которому Valet будет искать сайты.

    export PATH=$PATH:~/.composer/vendor/bin
    mkdir ~/Sites
    cd ~/Sites
    valet park

Создать ссылку на проект

    valet use php@8.0
    valet link drland
    valet restart

    brew services start mysql


Список

    valet links
