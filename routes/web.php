<?php

use App\Helper;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PromtsController;
use Illuminate\Support\Facades\Route;


// Dashboard
Route::get('/', [DashboardController::class, 'index'])
    ->name('dashboard');

// ->middleware('telegram.network')
