<?php
namespace App\Ai;
use App\Helper;
use App\Models\Promt;
use App\Models\Task;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Request;
use function Psy\debug;

class Tg {
    public static function user($userId, $data = null){
        $user = User::where('tg_id', $userId)->first();
        if(!$user && $data) {
            $user = new User();
            $user->tg_id = $userId;
            $user->name = $data->getUsername();
            $user->email = $data->getUsername().'@aiqube.net';
            $user->first_name = $data->getFirstName();
            $user->last_name = $data->getLastName();
            $user->password = '';
            $user->save();
        }
        return $user;
    }



    public static function answerTask($taskId, $answer, $user, $chat){
        $chat_id = $chat->getId();
        if(!$task = Task::where('id', $taskId)->first() ) {
            $user->active_task_id = null;
            $user->save();

            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text'=>'Задача отменена или удалена' ,
            ]);
        }



        $conf = json_decode($task->command, true);
        Log::debug($conf);

        $conf['answers'] = $conf['answers'] ?? [];
        $scheme = $conf['steps'] ?? [];


        $currentStep = isset($conf['stepCurrent']) ? (int)$conf['stepCurrent'] : 0;
        $currentScheme = $scheme[$currentStep];
        $propName = $currentScheme['var'];


        $conf['answers'][$propName] = $answer;


        if(isset($scheme[$currentStep+1])) {
            $conf['stepCurrent'] = $currentStep+1;

            $task->step = $conf['stepCurrent'];
            $task->command = json_encode($conf);
            $task->save();

            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text'=>$scheme[$currentStep+1]['msg'],
            ]);

        } else {
            $task->command = json_encode($conf);
            $task->save();
            $task->getResult();
        }

        return Request::emptyResponse();

    }
    public static function runTask($data, $user, $chat){

        $chat_id = $chat->getId();


        if(!is_array($data)) $data = json_decode($data, true);

        $code = $data['code'];
        $id = $data['id'];


        if($code == 'promt') {

            $promt = Promt::where('id', $id)->first();

            if(!$promt) {
                return [
                    'message'=>'Задача не найдена',
                    'status'=>'error'
                ];
            }

            $task = Task::createTask($chat_id, $user, $code, $promt );
            $user->active_task_id = $task->id;
            $user->save();
            Request::sendMessage([
                'chat_id' => $chat_id,
                'text'=>$promt->first_message
            ]);
        }

        if($code == 'task') {

        }


        die();
    }

    public static function sendMessage($chat_id, $text){
        Request::sendMessage([
            'chat_id' => $chat_id,
            'text'=>$text
        ]);
    }
    public static function sendPhoto($chat_id, $base){

        $res = Request::sendDocument([
            'chat_id' => $chat_id,
            'document'=>$base
        ]);

        dd($res);
    }



}
