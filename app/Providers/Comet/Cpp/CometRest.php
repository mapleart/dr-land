<?php

namespace App\Providers\Comet\Cpp;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CometRest {

    /**
     */
    private $connection;
    public $channels;

    public function __construct($config)
    {


        $server = $config['server'];
        $db = $config['db'];
        $user = $config['dev_id'];
        $password =$config['key'];

        try {
            $dsn = "mysql:host={$server};dbname={$db};charset=utf8";
            $opt = [
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            $this->connection = new \PDO($dsn, $user, $password, $opt);
        } catch (\PDOException $e) {
            die('Подключение не удалось: ' . $e->getMessage());
        }



    }

    static function getJWT($data)
    {
        $pass = Config::get('broadcasting.connections.comet.key');
        $dev_id = (int)Config::get('broadcasting.connections.comet.dev_id', 0);

        /**
         * Create token header as a JSON string
         */
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        if (isset($data['user_id'])) {
            $data['user_id'] = (int)$data['user_id'];
        }

        /**
         * Create token payload as a JSON string
         */
        $payload = json_encode($data);

        /**
         * Encode Header to Base64Url String
         */
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        /**
         * Encode Payload to Base64Url String
         */
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        /**
         * Create Signature Hash
         */
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $pass . $dev_id, true);

        /**
         * Encode Signature to Base64Url String
         */
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        /**
         * Create JWT
         */
        return trim($base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature);
    }

    static function pdoSet($allowed, &$values, $source = array()) {
        $set = '';
        $values = array();
        if (!$source) $source = &$_POST;
        foreach ($allowed as $field) {
            if (isset($source[$field])) {
                $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
                $values[$field] = $source[$field];
            }
        }
        return substr($set, 0, -2);
    }


    public function publishPrivate($channel, $event, $payload){
        $userId=$payload['user_id'];
        // У личных сообщений канал всегда msg !!!
        $data = [
            'id'=>$userId, //str_replace(':', '-', $channel),
            'event'=>$event,
            'message'=> isset($payload['payload']) ? json_encode($payload['payload']) : json_encode($payload),
        ];

        try {
            $sql = "INSERT INTO users_messages (id, event, message) VALUES (:id, :event, :message)";
            $this->connection->prepare($sql)->execute($data);
        } catch (\Exception $exception){

        }
    }


    public function publish($channel, $event, $payload){
        if(!$this->connection) return false;

        $data = [
            'name'=>str_replace(':', '-', $channel),
            'event'=>$event,
            'message'=> isset($payload['payload']) ? json_encode($payload['payload']) : json_encode($payload),
        ];

        try {
            $sql = "INSERT INTO pipes_messages (name, event, message) VALUES (:name, :event, :message)";
            $this->connection->prepare($sql)->execute($data);
        } catch (\Exception $exception){

        }
        // $this->connection->query('INSERT INTO pipes_messages (name, event, message)VALUES('web_MainPageChat', '', '{ \"text\":\"My text\",\"name\":\"My name\"}' );"');
    }
}
