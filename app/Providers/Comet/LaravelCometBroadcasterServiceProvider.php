<?php

namespace App\Providers\Comet;

use App\Providers\Comet\Cpp\CometRest;
use Illuminate\Broadcasting\BroadcastManager;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Arr;

class LaravelCometBroadcasterServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     */
    public function boot(BroadcastManager $broadcastManager)
    {

        $broadcastManager->extend('comet', function ($broadcasting, $config) {
            return new CometBroadcaster(
                new CometRest($config)
            );
        });
    }


}
