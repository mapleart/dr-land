<?php

namespace App\Providers\Comet;

use Illuminate\Broadcasting\Broadcasters\Broadcaster;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Providers\Comet\Cpp\CometRest;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CometBroadcaster extends Broadcaster
{
    use UseCometChannelConventions;

    /**
     * The CometRest SDK instance.
     *
     * @var \App\Providers\Comet\Cpp\CometRest
     */
    protected $comet;

    /**
     * Create a new broadcaster instance.
     *
     * @param  \App\Providers\Comet\Cpp\CometRest  $comet
     * @return void
     */
    public function __construct(CometRest $comet)
    {
        $this->comet = $comet;
    }

    /**
     * Authenticate the incoming request for a given channel.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function auth($request)
    {

        $channelName = $this->normalizeChannelName($request->channel_name);

        if ($this->isGuardedChannel($request->channel_name) &&
            ! $this->retrieveUser($request, $channelName)) {
            throw new AccessDeniedHttpException;
        }

        return parent::verifyUserCanAccessChannel(
            $request, $channelName
        );
    }

    /**
     * Return the valid authentication response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $result
     * @return mixed
     */
    public function validAuthenticationResponse($request, $result)
    {
        if (Str::startsWith($request->channel_name, 'private')) {
            $signature = $this->generateCometSignature($request->channel_name, $request->socket_id);
            return ['auth' => $this->getPublicToken() . ':' . $signature];
        }

        $channelName = $this->normalizeChannelName($request->channel_name);
        $userId = $this->retrieveUser($request, $channelName)->getAuthIdentifier();

        $userData = [
            'user_id' => $userId,
        ];

        if ($result) {
            $userData['user_info'] = $result;
        }

        $signature = $this->generateCometSignature($request->channel_name, $request->socket_id, $userData);

        return [
            'auth' => $this->getPublicToken() . ':' . $signature,
            'channel_data' => json_encode($userData),
        ];
    }

    /**
     * Generate the Signature for Comet auth headers
     * @param $channelName
     * @param $socketId
     * @param null $userData
     * @return string
     */
    public function generateCometSignature($channelName, $socketId, $userData = null)
    {
        $privateToken = $this->getPrivateToken();

        $signature = $socketId .':' . $channelName;

        if ($userData) {
            $signature .= ':' . json_encode($userData);
        }

        return hash_hmac('sha256',$signature, $privateToken);
    }

    /**
     * Broadcast the given event.
     *
     * @param  array  $channels
     * @param  string  $event
     * @param  array  $payload
     * @return void
     */
    public function broadcast(array $channels, $event, array $payload = [])
    {
        foreach (self::formatChannels($channels) as $channel) {
            $data = explode(':', $channel);
            $private =$data[0] === 'private';
            $channel=$data[1];

            if($private) {
                $this->comet->publishPrivate( $channel, $event, $payload);
            } {
                $this->comet->publish($channel, $event, $payload);
            }
        }
    }

    /**
     * Get the Public Token Value out of the Comet key
     * @return mixed
     */
    public function getPublicToken()
    {
        return Str::before($this->comet->options->key, ':');
    }

    /**
     * Get the Private Token value out of the Comet Key
     * @return mixed
     */
    public function getPrivateToken()
    {
        return Str::after($this->comet->options->key, ':');
    }

    /**
     * Get the Comet SDK instance.
     *
     * @return \App\Providers\Comet\Cpp\CometRest
     */
    public function getComet()
    {
        return $this->comet;
    }
}
