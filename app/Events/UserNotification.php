<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class UserNotification implements ShouldBroadcastNow {
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $event;
    public $payload;
    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($event, $userId, $data)
    {
        $this->event = $event;
        $this->payload = $data;
        $this->user_id = $userId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('user-notification');
    }
    public function broadcastAs()
    {
        return $this->event;
    }
}
