<?php

namespace App\Http\Controllers;
use App\Classes\ClanDaoDB;
use App\Classes\Stats;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index() {
        return Inertia::render('Dashboard/Index', [
            'success'=>1
        ]);
    }
}
