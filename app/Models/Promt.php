<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promt extends Model
{
    use HasFactory;

    public function Formalyze(){


        return array(
            'id'=>(int)$this->id,
            'title'=>$this->title,
            'code'=>$this->code,
            'first_message'=>$this->first_message,
        );
    }

    public function toArray()
    {
        $res = parent::toArray();
        return array_merge($res ,[
            'href_constructor'=>'/promt/'.$this->id.'/constructor/',
            'href_run'=>'/promt/'.$this->id.'/run/',
            'steps'=>json_decode($this->steps, true),
            'resource'=>json_decode($this->resource, true),
        ]);
    }
}
