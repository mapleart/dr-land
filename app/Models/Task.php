<?php

namespace App\Models;

use App\Ai\Ai;
use App\Helper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Longman\TelegramBot\Request;

class Task extends Model
{
    use HasFactory;

    public function replyTg($text){
        if(!$chatId=$this->chat_id) {
            return false;
        }

        Request::sendMessage([
            'chat_id' => $chatId,
            'text'=>$text
        ]);
    }

    public function getResult(){

        $command = json_decode($this->command, true);
        $code  = $command['code'];
        $vars  = $command['answers'];

        if($code == 'people') {

            $promt = 'Сейчас я хочу, чтобы ты придумал вымышленную личность, используя реальные данные из сети. Тебе нужно создать полный цифровой профиль человека, живущего в '.$vars['Country'].', работающего в сфере маркетинга, занимающегося рекламой.

Профиль должен содержать:

1. Имя Фамилию
2. Дата рождения
3. Пол
4. Небольшое bio: где учился, работал, увлечения и профессиональные интересы
5. Домашний адрес в '.$vars['City'].'
6. Список социальных сетей и профессиональных сервисов, соответствующих его профессиональному профилю, например: twitter, linkedin, discord, reddit, facebook и тд.
7. Bio для профилей twitter
8. Bio для профиля linkedin

';


            $this->replyTg('Необходимые данные собраны, операция выполняется...');
           $resText = Ai::GptRequest($promt);

            $this->replyTg($resText);

            $photoPromt = Ai::GptRequest(' На основе биографии '.$resText.' создай PROMT для DALL-E длинной 500 символов');
            $this->replyTg($photoPromt);


            $url = Ai::DalleRequest($photoPromt);

            $this->replyTg('Генерируем фото...');
            $this->replyTg($url);

            $user = User::where('id', $this->user_id)->first();
            $user->active_task_id = null;
            $user->save();

            $this->replyTg('Завершено');

        } else {
            Helper::debug($code);
            Helper::debug($vars);
        }
    }
    static public function createTask($chat_id, $user, $code, $command){
        $task = new Task();
        $task->code = $code;
        $task->chat_id = $chat_id;
        $task->command = json_encode($command);
        $task->user_id = $user->id;
        $task->tg_user_id = $user->tg_id;
        $task->save();

        return $task;
    }
}
