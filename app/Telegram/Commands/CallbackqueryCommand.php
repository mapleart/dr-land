<?php


namespace PhpTelegramBot\Laravel\Telegram\Commands;


use App\Ai\Ai;
use App\Ai\Tg;
use App\Helper;
use Illuminate\Support\Facades\App;
use Longman\TelegramBot\Commands\Command;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use PhpTelegramBot\Laravel\Facades\Telegram;
use PhpTelegramBot\Laravel\Telegram\Conversation\ConversationWrapper;
use PhpTelegramBot\Laravel\Telegram\InlineKeyboardButton\RemembersCallbackPayload;
use PhpTelegramBot\Laravel\Telegram\UsesEffectiveEntities;
use function Psy\debug;

class CallbackqueryCommand extends SystemCommand
{
    use RemembersCallbackPayload, UsesEffectiveEntities;

    protected $name = 'callbackquery';
    protected $description = 'Handles CallbackQueries';
    protected $version = '1.0';

    public function execute(): ServerResponse
    {
        $chat = $this->getEffectiveChat($this->getUpdate());

        $getCallbackQuery = $this->getCallbackQuery();
        $from = $getCallbackQuery->getFrom();
        $user_id = $from->getId();

        $data = $getCallbackQuery->getData();

        $message = $this->getMessage();
        //$command = $message->getCommand();
        //$txt = $message->getText();)



        $user = Tg::user($user_id, $from);

        $task = Tg::runTask($data, $user, $chat);


        $chat_id = $chat->getId();

        if($task['status'] == 'error') {

            return Request::sendMessage( [
                'chat_id'      => $chat_id,
                'text'         => $task['message']
            ]);
        }

        return Request::emptyResponse();
    }
}
