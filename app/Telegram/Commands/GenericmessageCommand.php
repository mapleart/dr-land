<?php


namespace App\Telegram\Commands;


use App\Ai\Tg;
use App\Helper;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Exception\TelegramException;

class GenericmessageCommand extends SystemCommand
{

    /** @var string Command name */
    protected $name = 'genericmessage';
    /** @var string Command description */
    protected $description = '';
    /** @var string Usage description */
  //  protected $usage = '/genericmessage';
    /** @var string Version */
    protected $version = '1.0.0';

    public function execute(): ServerResponse
    {

        $message = $this->getMessage();
        $user_id = $message->getFrom()->getId();
        $command = $message->getCommand();
        $text = $message->getText();



        ////
        ////
        ////

        $user = Tg::user($user_id, $message->getFrom());
        if($user->active_task_id){
            return Tg::answerTask($user->active_task_id, $text, $user, $message->getChat());
            return '';
        }


        // To enable proper use of the /whois command.
        // If the user is an admin and the command is in the format "/whoisXYZ", call the /whois command
        if (stripos($command, 'whois') === 0 && $this->telegram->isAdmin($user_id)) {
            return $this->telegram->executeCommand('whois');
        }


        $OPENAI_API_KEY = "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";
        $sModel = "gpt-3.5-turbo";
        $ch = curl_init();
        $headers  = [
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $OPENAI_API_KEY . ''
        ];



        $postData = [
            'model' => $sModel,
            'messages'=>[
                [
                    'role'=>'user',
                    'content'=>$text,
                ]
            ],
        ];

        curl_setopt($ch, CURLOPT_URL, 'https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));


        $result = curl_exec($ch);
        $decoded_json = json_decode($result, true);
        return $this->replyToChat($decoded_json['choices'][0]['message']['content']);
    }

}
