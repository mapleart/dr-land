<?php

namespace App;
class Helper {
    public static function folderSize($dir){
        if(!is_dir($dir)) return 0;
         $totalsize=0;
        if ($dirstream = @opendir($dir)) {
            while (false !== ($filename = readdir($dirstream))) {
                if ($filename!="." && $filename!="..")
                {
                    if (is_file($dir."/".$filename))
                        $totalsize+=filesize($dir."/".$filename);

                    if (is_dir($dir."/".$filename))
                        $totalsize+=self::folderSize($dir."/".$filename);
                }
            }
        }
        closedir($dirstream);
        if($totalsize > 0) return $totalsize / 1024;
        return 0;
    }



    // функция форматирует вывод размера
    public static function  formatSize($size){
        $metrics[0] = 'байт';
        $metrics[1] = 'Кбайт';
        $metrics[2] = 'Мбайт';
        $metrics[3] = 'Гбайт';
        $metrics[4] = 'Тбайт';
        $metric = 0;
        while(floor($size/1024) > 0){
            ++$metric;
            $size /= 1024;
        }
        $ret =  round($size,1)." ".(isset($metrics[$metric])?$metrics[$metric]:'??');
        return $ret;
    }

    const sendLog = true;
    public static function debug($text, $title=''){
        if(!self::sendLog) return false;
        $TG_LOG_USERS = '410092998';
        $TG_LOG_TOKEN = config('telegram.bot.api_token');

        if(!is_scalar($text)) {
            $text = json_encode($text, JSON_UNESCAPED_UNICODE).PHP_EOL;
        }
        self::sendTelegramMessage($text, $title, $TG_LOG_USERS, $TG_LOG_TOKEN);
    }

    static public function sendTelegramMessage($text, $title=null, $TG_LOG_USERS=null, $TG_LOG_TOKEN=null){
        if(!$TG_LOG_USERS or !$TG_LOG_TOKEN) return;

        $text = strip_tags($text, '<strong>,<p>');
        if( $title ) $text = '<strong>'.$title.'</strong>'.PHP_EOL.PHP_EOL . $text . PHP_EOL.PHP_EOL.PHP_EOL;

        $users = explode(',', $TG_LOG_USERS);
        $curl = curl_init();
        foreach ($users as $userId) {
            $url = "https://api.telegram.org/bot" . $TG_LOG_TOKEN . "/sendMessage?chat_id=" . $userId;
            $url = $url . "&text=" . urlencode($text) . "&parse_mode=html";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true
            ));
            $result = curl_exec($curl);
            curl_close($curl);
        }
    }
}
