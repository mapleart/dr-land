<?php
namespace App\Classes;

use App\Orchid\Layouts\User\UserRoleLayout;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Auth\Factory as Auth;
use Orchid\Platform\Models\Role;

class UserMenu {

    private $menu = array();
    private $user = array();
    public function __construct($user)
    {

        $this->user = $user;
        $this->add('dashboard', 'Dashboard', ['to'=>'/', 'icon'=>'bi bi-house'], []);
        $this->add('promts', 'Promts', ['to'=>'/promts', 'icon'=>'bi bi-braces'], ['dashboard.promts']);

       // $this->add('peoples', 'Пользователи', ['to'=>'/dashboard/peoples', 'icon'=>'dr-icon-notifications'], ['dashboard.inquire'], ['manager_%%'=>'like']);
    }

    /**
     * Добавляет меню
     * @param $code
     * @param $title
     * @param $params
     * @param array $permissions
     * @param array $groups
     */
    public function add($code, $title, $params, $permissions=[], $roles=[]) {
        $this->menu[$code]=[
            'code'=>$code,
            'icon'=>$params['icon'] ?? '',
            'title'=>$title,
            'link'=>$params,
            'permissions'=>$permissions,
            'roles'=>$roles,
        ];
    }

    /**
     * Возвращает меню
     */
    public function buildJs(){
        $auth = Auth();
        $auth->shouldUse(config('platform.guard'));

        $result = [];
        foreach ($this->menu as $menu){
            $checkP = $this->checkPermissions($menu['permissions']);
            $checkR = $this->checkRoles($menu['roles']);

            if($checkP && $checkR) $result[] = $menu;
        }

        return $result;
    }

    public function checkPermissions($permissions){
        if(!count($permissions)) return true;

        if(!$this->user) return false;

        $bResult = true;
        foreach ($permissions as $permission){
            if (!$this->user->hasAccess($permission)) {
                $bResult = false;
            }
        }


        return $bResult;
    }

    public function checkRoles($roles){
        if(!count($roles)) return true;

        $userRoles = $this->roles()->pluck('role_id')->all();
        $bResult = true;
        foreach ($roles as $role => $operator) {
            switch ($operator) {
                case 'like':
                    $findRoles = Role::where('slug', 'like', $role)->pluck('id')->all();
                    $correctRoles = array_uintersect($findRoles, $userRoles, 'strcasecmp');

                    if(!count($correctRoles)) $bResult = false;
                    break;
                default:
                    $findRoles = Role::where('slug', $role)->pluck('id')->all();
                    $correctRoles = array_uintersect($findRoles, $userRoles, 'strcasecmp');
                    if(!count($correctRoles)) $bResult = false;
            }
        }
        return $bResult;
    }
}

