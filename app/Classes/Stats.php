<?php
namespace App\Classes;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Self_;

class Stats {

    public static $error = false;
    public static $errorMessage = '';
    /*
     *
     * --- Предустановленные временные интервалы ---
     *
     */


    /*
     * Вчера
     */
    const GROUP_DATE_YESTERDAY = 'yesterday';
    /*
     * Сегодня
     */
    const GROUP_DATE_TODAY = 'today';
    /*
     * Неделя
     */
    const GROUP_DATE_WEEK = 'week';
    /*
     * Месяц
     */
    const GROUP_DATE_MONTH = 'month';
    /*
     * Год
     */
    const GROUP_DATE_YEAR = 'year';

    /**
     * Возвращает периоды для построения выборки
     * @return array
     */
    public static function getPeriods() {
        return [
            self::GROUP_DATE_YESTERDAY=>'Вчера',
            self::GROUP_DATE_TODAY=>'Сегодня',
            self::GROUP_DATE_WEEK=>'Неделя',
            self::GROUP_DATE_MONTH=>'Месяц',
            self::GROUP_DATE_YEAR=>'Год',
        ];
    }

    /**
     * Возвращает период поумолчанию
     * @return string
     */
    public static function getPeriodDefault() {
        return self::GROUP_DATE_MONTH;
    }

    /**
     * Получить реальный временной интервал в зависимости от типа периода для статистики
     *
     * @param $sPeriod  string тип периода
     * @return array array('from' => '...', 'to' => '...', 'format' => '...', 'interval' => '...')
     */
    protected static function getPeriod($sPeriod = null)
    {
        switch ($sPeriod) {
            /*
             * вчера
             */
            case self::GROUP_DATE_YESTERDAY:
                $iTime = mktime(date('H'), date('i'), date('s'), date('n'), date('j') - 1, date('Y'));
                return array(
                    'from'     => date('Y-m-d 00:00:00', $iTime),
                    'to'       => date('Y-m-d 23:59:59', $iTime),
                    /*
                     * Для одноденных периодов нужны интервалы по пол часа, поэтому в формате указаны часы и минуты.
                     * Убираем ненужные данные (полный 'Y-m-d H:i:00') чтобы подписи влезли, все равно имеем дело с известным интервалом
                     */
                    'format'   => 'H:i',
                    /*
                     * интервал для периода 30 мин
                     * tip: используется формат дат для strtotime
                     */
                    'interval' => '+30 minutes'
                );
                break;
            /*
             * сегодня
             */
            case self::GROUP_DATE_TODAY:
                return array(
                    'from'     => date('Y-m-d 00:00:00'),
                    'to'       => date('Y-m-d 23:59:59'),
                    /*
                     * Для одноденных периодов нужны интервалы по пол часа, поэтому в формате указаны часы и минуты.
                     * Убираем ненужные данные (полный 'Y-m-d H:i:00') чтобы подписи влезли, все равно имеем дело с известным интервалом
                     */
                    'format'   => 'H:i',
                    /*
                     * интервал для периода 30 мин
                     */
                    'interval' => '+30 minutes'
                );
                break;
            /*
             * неделя
             */
            case self::GROUP_DATE_WEEK:
                return array(
                    /*
                     * полных 7 дней назад (не включая текущий)
                     */
                    'from'     => date('Y-m-d',
                        mktime(date('H'), date('i'), date('s'), date('n'), date('j') - 7, date('Y'))),
                    'to'       => date('Y-m-d 23:59:59',
                        mktime(date('H'), date('i'), date('s'), date('n'), date('j') - 1, date('Y'))),
                    /*
                     * Для больших периодов интервал 1 день, поэтому часы и меньшие значения не указаны в формате.
                     * Убираем ненужные данные (полный 'Y-m-d') чтобы подписи влезли, все равно имеем дело с известным интервалом
                     */
                    'format'   => 'm-d',
                    /*
                     * интервал для периода 1 день
                     */
                    'interval' => '+1 day'
                );
                break;
                /*
                 * Год
                 */
            case self::GROUP_DATE_YEAR:
                return array(
                    'from'     => date('Y-m-d',
                        mktime(date('H'), date('i'), date('s'), date('n')+1, date('j'), date('Y') - 1 )),
                    'to'       => date('Y-m-d 23:59:59',
                        mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y'))),
                    /*
                     * Для больших периодов интервал 1 день, поэтому часы и меньшие значения не указаны в формате.
                     * Убираем ненужные данные (полный 'Y-m-d') чтобы подписи влезли, все равно имеем дело с известным интервалом
                     */
                    'format'   => 'Y-m',
                    /*
                     * интервал для периода 1 день
                     */
                    'interval' => '+1 month'
                );
                break;
            /*
             * месяц
             */
            case self::GROUP_DATE_MONTH:
                /*
                 * используется период по-умолчанию
                 */
                //break;
                /*
                 * период по-умолчанию
                 */
            default:
                return array(
                    'from'     => date('Y-m-d',
                        mktime(date('H'), date('i'), date('s'), date('n') - 1, date('j'), date('Y'))),
                    'to'       => date('Y-m-d 23:59:59',
                        mktime(date('H'), date('i'), date('s'), date('n'), date('j') - 1, date('Y'))),
                    /*
                     * Для больших периодов интервал 1 день, поэтому часы и меньшие значения не указаны в формате.
                     * Убираем ненужные данные (полный 'Y-m-d') чтобы подписи влезли, все равно имеем дело с известным интервалом
                     */
                    'format'   => 'm-d',
                    /*
                     * интервал для периода 1 день
                     */
                    'interval' => '+1 day'
                );
                break;
        }
    }


    /**
     * Заполнить пустыми значениями период дат с нужным для каждого периода интервалом
     *
     * @param $aPeriod array    период дат (от и до) и другие данные
     * @return array            массив с нулевыми значениями на каждый промежуток интервала в периоде дат
     */
    public static function fillRangeForPeriod($aPeriod)
    {
         /*
         * интервал прохода по датам указанный как прирост для strtotime ("+1 day")
         */
        $sInterval = $aPeriod['interval'];
        /*
         * дата начала и счетчик
         */
        $iCurrentTime = strtotime($aPeriod['from']);
        /*
         * дата финиша
         */
        $iFinishTime = strtotime($aPeriod['to']);
        /*
         * здесь хранятся даты и количество
         */
        $aData = array();
        /*
         * заполнить пустыми значениями интервалы в периоде
         */
        do {
            /*
             * добавить запись про текущую дату
             */
            $dateStart = date($aPeriod['format'], $iCurrentTime);


            if($aPeriod['interval'] == '+1 day') {
                $time = strtotime(date('Y-m-d', $iCurrentTime));


                $date_from = date('Y-m-d 00:00:00', $iCurrentTime);
                $date_to = date('Y-m-d 23:59:59', $iCurrentTime);

            } else {
                $time = strtotime(date('Y-m-01', $iCurrentTime));

                $date_from = date('Y-m-01 00:00:00', $iCurrentTime);
                $date_to = date('Y-m-d 23:59:59',
                    mktime(date('H', $time), date('i', $time), date('s', $time), date('n', $time)+1, date('j', $time)-1, date('Y', $time)));

            }


            $aData[$dateStart] = array(
                /*
                 * формат даты берется из периода, где был задан её формат связанный с интервалом
                 */
                'date'  => $dateStart,
                'date_from' => $date_from,
                'date_to' => $date_to,
                'count' => 0
            );
            /*
             * увеличить интервал
             */
            $iCurrentTime = strtotime($sInterval, $iCurrentTime);
        } while ($iCurrentTime <= $iFinishTime);
        return $aData;
    }


    /**
     * Заполнить реальными данными из запроса период
     *
     * @param $aFilledWithZerosPeriods "пустые" данные периода для каждой даты
     * @param $aDataStats                    полученные данные для дат
     * @return array                        объедененный массив данных
     */
    protected function MixEmptyPeriodsWithData($aFilledWithZerosPeriods, $aDataStats)
    {
        if (!is_array($aFilledWithZerosPeriods) or !is_array($aDataStats)) {
            return array();
        }
        foreach ($aFilledWithZerosPeriods as &$aFilledPeriod) {
            foreach ($aDataStats as $aData) {
                /*
                 * если есть реальные данные для этой даты
                 */
                if ($aFilledPeriod['date'] == $aData['date']) {
                    $aFilledPeriod['count'] = $aData['count'];
                }
            }
        }
        return $aFilledWithZerosPeriods;
    }


    /**
     * Получить данные периода при ручном выборе дат
     *
     * @param $sDateStart string дата начала
     * @param $sDateFinish string дата финиша
     * @return array
     */
    protected static function setupCustomPeriod($sDateStart, $sDateFinish)
    {
        $aPeriod = self::getPeriod();
        /*
         * самое начало суток
         */
        $aPeriod['from'] = date('Y-m-d 00:00:00', strtotime($sDateStart));
        /*
         * верхний предел должен быть указан с точностью до секунды и максимальным по времени
         * т.к. если регистрация в последнем дне выбранного периода была не в 0 часов, 0 минут и 0 секунд - то она не будет учтена,
         * т.к. мускульный формат date будет сконвертирован в datetime с постфиксом 00:00:00
         * поэтому нужно указать макс. время суток вручную
         */
        $aPeriod['to'] = date('Y-m-d 23:59:59', strtotime($sDateFinish));
        /*
         *
         * группировка для очень больших выбранных периодов
         *
         */
        /*
         * если диапазон больше 1 месяца, то группируем по неделям
         */
        if (strtotime('+1 month', strtotime($sDateStart)) <= strtotime($sDateFinish)) {
            /*
             * формат с указанием недели года в скобках
             */
            $aPeriod['format'] = 'Y (W)';
            /*
             * интервал для периода - 1 неделя
             * tip: используется формат дат для strtotime
             */
            $aPeriod['interval'] = '+1 week';
        }
        /*
         * если диапазон больше 5 месяцев, то группируем по месяцам
         */
        if (strtotime('+5 month', strtotime($sDateStart)) <= strtotime($sDateFinish)) {
            $aPeriod['format'] = 'Y-m';
            $aPeriod['interval'] = '+1 month';
        }
        return $aPeriod;
    }


    /**
     * Отконвертировать описание форматирования даты из php в mysql
     *
     * @param $sFormat string форматирования как в php ("Y-m-d")
     * @return mixed string для мускула ("%Y-%m-%d")
     */
    public static function convertToMysqlDate($sFormat)
    {
        /*
         * таблица соответствий формата даты из php (date) в mysql (DATE_FORMAT)
         * docs: http://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html#function_date-format
         */
        $aDateFormatConvertation = array(
            /*
             * прямая конвертация
             */
            'Y' => '%Y',
            'm' => '%m',
            'd' => '%d',
            'H' => '%H',
            /*
             * недели для пхп и мускула отличаются
             */
            'W' => '%u',
            /*
             * хак: если формат даты содежит минуты, то чтобы не округлять даты в бд к получасам (ведь действие может быть и в 55 минут) -
             * ставим минуты в 30 как среднее и привязываем действие только к часу (т.е. каждое действие будет в Х часов 30 минут для интервалов, где указаны минуты)
             */
            'i' => '30',
        );
        return strtr($sFormat, $aDateFormatConvertation);
    }


    /**
     * Получить данные для графика
     *
     * @param null $sGraphPeriod именованный период графика
     * @param null $sDateStart дата начала периода
     * @param null $sDateFinish дата окончания периода
     */
    public static function buildStats($sGraphPeriod = null, $sDateStart = null, $sDateFinish = null ) {
        self::$error = false;

        /*
         * тип периода для графика
         */
        if (!in_array($sGraphPeriod, array(
            self::GROUP_DATE_YESTERDAY,
            self::GROUP_DATE_TODAY,
            self::GROUP_DATE_WEEK,
            self::GROUP_DATE_MONTH,
            self::GROUP_DATE_YEAR
        ))
        ) {
            $sGraphPeriod = self::GROUP_DATE_MONTH;
        }


        /*
         * если указан ручной интервал дат
         */
        if ($sDateStart and $sDateFinish) {
            /*
             * валидация дат
             */
            if (!self::validateStartAndFinishDates($sDateStart, $sDateFinish)) {
                self::$error = true;
                self::$errorMessage = 'Неверный формат даты';
                return false;
            } else {
                /*
                 * проверить чтобы дата начала была меньше чем дата конца
                 */
                if (strtotime($sDateStart) > strtotime($sDateFinish)) {
                    self::$error = true;
                    self::$errorMessage = 'Дата начала больше чем дата конца';

                    return false;

                } else {
                    /*
                     * построить данные о периоде
                     */
                    $aPeriod = self::setupCustomPeriod($sDateStart, $sDateFinish);
                    $sGraphPeriod = null;
                }
            }
        }

        /*
         *
         * Рассчет данных
         *
         */

        /*
         * получить период дат от и до для названия интервала если не был выбран ручной интервал дат
         */
        if (!isset($aPeriod)) {
            $aPeriod = self::getPeriod($sGraphPeriod);
        }
        /*
         * получить пустой интервал дат для графика
         */
        //$aFilledWithZerosPeriods = $this->FillDatesRangeForPeriod($aPeriod);
        /*
         * получить существующие данные о типе
         */
        //$aDataStats = $this->GetStatsDataForGraphCorrespondingOnType($sGraphType, $aPeriod);
        /*
         * объеденить данные с пустым интервалом (чтобы исключить "дыры" в промежутке)
         */
        //$aDataStats = $this->MixEmptyPeriodsWithData($aFilledWithZerosPeriods, $aDataStats);
        /*
         * получить шаг каждой показываемой подписи к графику (чтобы убрать лишние, если их слишком много)
         */
        //$iPointsStepForLabels = $this->GetPointsStepForLabels($aDataStats);

        return $aPeriod;

    }


    /**
     * Проверить корректность дат начала и конца ручного выбранного периода для графика
     *
     * @param $sDateStart string дата начала
     * @param $sDateFinish string дата финиша
     * @return bool корректность дат
     */
    protected static function validateStartAndFinishDates($sDateStart, $sDateFinish)
    {
        /*
         * параметры для валидатора дат: формат даты мускула date и datetime
         */
        $validator = Validator::make([
            'date_start'=>$sDateStart,
            'date_finish'=>$sDateFinish,
        ], [
            'date_start'=>'required|date_format:yyyy-MM-dd hh:mm:ss',
            'date_finish'=>'required|date_format:yyyy-MM-dd hh:mm:ss'
        ]);

        return true;

    }


    /**
     * Получить шаг для подписей графика (если записей графика слишком много, то они просто не влезают в подписи к графику и нужно посчитать какую каждую подпись следует выводить)
     *
     * @param $aDataStats        собранные данные графика
     * @return int                шаг для вывода подписей точек графика
     */
    protected function GetPointsStepForLabels($aDataStats)
    {
        /*
         * если точек больше, чем нужно
         */
        if (count($aDataStats) > Config::Get('plugin.admin.stats.max_points_on_graph')) {
            /*
             * подсчитать во сколько раз больше точек, чем нужно
             */
            return (int)round(count($aDataStats) / Config::Get('plugin.admin.stats.max_points_on_graph'));
        }
        /*
         * выводить каждую точку
         */
        return 1;
    }


    /**
     * Получить количество объектов в прошлом и текущем периоде, прирост объектов за период (на сколько больше зарегистрировалось в текущем периоде чем в прошлом),
     * линейку голосов и рейтингов для объектов
     *
     * @param $aGrowthFilter   array фильтр
     * @param $sPeriod         тип периода
     * @return array           array('growth' => прирост объектов)
     */
    public static function growth($aGrowthFilter, $sPeriod = null, $connection = '', $title='', $data=[] ) {
        $aPeriod = self::growthQueryRuleForPeriod($sPeriod);

        /*
         * получить значение количества объектов в прошлом и текущем периоде
         */
        $aDataGrowth = self::selectGrowthByFilterAndPeriod($aGrowthFilter, $aPeriod, $connection);

        $percent = 0;
        $def = $aDataGrowth['now'] - $aDataGrowth['prev'];
        if( $aDataGrowth['prev'] <= 0 ) {
            $percent = $aDataGrowth['now'] * 100;
        } else if (abs($def) > 0 ) {
            $percent = ( $aDataGrowth['now'] - $aDataGrowth['prev'] ) / $aDataGrowth['prev'] * 100;
        }

        $class=$data['class'] ?? '';
        $icon=$data['icon'] ?? '';
        return array(
            'title'=>$title,
            'class'=>$class,
            'icon'=>$icon,
            /*
             * количество объектов в прошлом периоде
             */
            'prev' => $aDataGrowth['prev'],
            /*
             * объектов в текущем периоде
             */
            'now'  => $aDataGrowth['now'],
            /*
             * прирост
             */
            'growth'     => $aDataGrowth['now'] - $aDataGrowth['prev'],
            'growth_percent'     => $percent

        );
    }


    /**
     * Получить части sql-запросов периода для выбора данных из БД по имени периода
     *
     * @param $sPeriod именованое название периода
     * @return array массив с датой "от" и "до"
     */
    protected static function growthQueryRuleForPeriod($sPeriod)
    {
        switch ($sPeriod) {
            /*
             * вчера
             */
            case self::GROUP_DATE_YESTERDAY:
                return array(
                    'now_period'  => 'BETWEEN CURRENT_DATE - INTERVAL 1 DAY AND CURRENT_DATE',
                    'prev_period' => 'BETWEEN CURRENT_DATE - INTERVAL 2 DAY AND CURRENT_DATE - INTERVAL 1 DAY'
                );
            /*
             * неделя
             */
            case self::GROUP_DATE_WEEK:
                return array(
                    'now_period'  => 'BETWEEN CURRENT_DATE - INTERVAL 1 WEEK AND CURRENT_DATE',
                    'prev_period' => 'BETWEEN CURRENT_DATE - INTERVAL 2 WEEK AND CURRENT_DATE - INTERVAL 1 WEEK'
                );
            /*
             * месяц
             */
            case self::GROUP_DATE_MONTH:
                return array(
                    'now_period'  => 'BETWEEN CURRENT_DATE - INTERVAL 1 MONTH AND CURRENT_DATE',
                    'prev_period' => 'BETWEEN CURRENT_DATE - INTERVAL 2 MONTH AND CURRENT_DATE - INTERVAL 1 MONTH'
                );

            case self::GROUP_DATE_YEAR:
                return array(
                    'now_period'  => 'BETWEEN CURRENT_DATE - INTERVAL 1 YEAR AND CURRENT_DATE',
                    'prev_period' => 'BETWEEN CURRENT_DATE - INTERVAL 2 YEAR AND CURRENT_DATE - INTERVAL 1 YEAR'
                );
            /*
             * сегодня
             */
            case self::GROUP_DATE_TODAY:
                /*
                 * использовать период по-умолчанию
                 */
                /*
                 * период по-умолчанию (1 день - сегодня)
                 */
            default:
                return array(
                    'now_period'  => 'BETWEEN CURRENT_DATE AND CURRENT_DATE + INTERVAL 1 DAY',
                    'prev_period' => 'BETWEEN CURRENT_DATE - INTERVAL 1 DAY AND CURRENT_DATE'
                );
        }
    }


    /**************************************************************************************************************************/

    /**
     * Получить прирост топиков, комментариев, блогов, пользователей за период
     *
     * @param $aFilter        фильтр
     * @param $aPeriod        периоды
     * @return array
     */
    public static function selectGrowthByFilterAndPeriod($aFilter, $aPeriod, $connection=null) {
        if(!$connection) {
            $connection=config('database.default');
        }

        $table = $aFilter['table'];
        $period = $aFilter['period_row_name'];
        $primary_key = $aFilter['primary_key'] ?? 'id';
        $where = $aFilter['where'] ?? [];
        $join = $aFilter['join'] ?? '';

        $sWhere = self::buildWhereQuery( $where );
        $sql = 'SELECT
			(
				SELECT COUNT( o.'.$primary_key.' )
				FROM
					`' . $table . '` as o
					 '.$join.'
				WHERE
					' . $sWhere . '
					AND
					o.`' . $period . '` ' . $aPeriod['now_period'] . '
			) as now,
			(
				SELECT COUNT( o.'.$primary_key.'  )
				FROM
					`' . $table . '` as o
					'.$join.'
				WHERE
					' . $sWhere . '
					AND
					o.`' . $period . '` ' . $aPeriod['prev_period'] . '
			) as prev
		';
        $result = DB::connection($connection)->select($sql);
        return (array)array_shift($result);
    }


    /**
     * Построить условие WHERE sql-запроса для получения прироста
     *
     * @param $aConditions        массив условий
     * @return string            часть sql-строки
     */
    protected static function buildWhereQuery($aConditions)
    {
        $sSql = '1 = 1';
        foreach ($aConditions as $sFieldRaw => $mValue) {
            /*
             * если поле указано с алиасом таблицы (o.`field_id`), то не экранировать его
             */
            $sField = strpos($sFieldRaw, '.') === false ?  DB::connection()->getPdo()->quote($sFieldRaw) : $sFieldRaw;
            $sSql .= ' AND ' . $sField . self::getCorrectSyntaxForValue($mValue);
        }
        return $sSql;
    }


    /**
     * Вернуть корректную запись для запроса в зависимости от типа значения
     *
     * @param $mValue        значение
     * @return string        часть sql-запроса WHERE
     * @throws Exception    не поддерживаемый тип переменной
     */
    protected static function getCorrectSyntaxForValue($mValue)
    {

        if($mValue == 'notnull') {
            return  ' IS NOT NULL';
        }
        switch (gettype($mValue)) {
            case 'float':
            case 'integer':
                return ' = ' . $mValue;
            case 'string':
                return ' = "' . $mValue . '"';
            case 'array':
                return ' IN ("' . implode('", "', $mValue) . '")';
            default:
                throw new \Exception('admin: error: unsupported value type in ' . __METHOD__ . ': ' . gettype($mValue));
        }
    }

}

