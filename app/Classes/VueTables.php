<?php
namespace App\Classes;

class VueTables {
    private $query = null;
    private $header = [];

    public function __construct($query)
    {
        $this->query = $query;
    }

    /**
     * Добавить колонку
     * @param $key
     * @param $name
     * @param $classes
     * @param false $sortable
     * @param string $tooltip
     * @return $this
     */
    public function addHeaderItem($key, $name, $classes='', $sortable=false, $tooltip='') {
        $this->header[$key]=[
            'name'=>$name,
            'classes'=>$classes,
            'sortable'=>$sortable,
            'description'=>$tooltip ];
        return $this;
    }

    /**
     * Отдает шапку таблицы
     * @return array
     */
    public function getHeader(){
        return $this->header;
    }

    public function applyFilters($filters) {
        foreach ( $filters as $filter ) {
            if(! ( isset($filter['value']) && !!$filter['value']) ) continue;
            switch ($filter['type']) {
                case 'select':
                    $this->query->whereIn($filter['name'], $filter['value']);
                    break;
            }
        }
    }

    public function count(){
        return $this->query->count();
    }
    public function selectPage($page, $perPage){
        return $this->query->offset( ($page-1) * $perPage)->take($perPage)->get();
    }


}